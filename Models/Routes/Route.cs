﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MetroChecker.Models.Routes
{
    public class Route
    {
        public ServiceStatus route_service_status { get; set; }
        public int route_type { get; set; }
        public int route_id { get; set; }
        public string route_name { get; set; }
        public string route_number { get; set; }
        public string route_gtfs_id { get; set; }
        public string[] geopath { get; set; }
    }
}
