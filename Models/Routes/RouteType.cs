﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MetroChecker.Models.Routes
{
    public class RouteType
    {
        public string route_type_name { get; set; }
        public int route_type { get; set; }
    }
}
