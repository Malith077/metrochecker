﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MetroChecker.Models.Routes
{
    public class Routes
    {
        public List<Route> routes { get; set; }
    }
}
