﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MetroChecker.Models.Routes
{
    public class ServiceStatus
    {
        public string description { get; set; }
        public DateTime timestamp { get; set; }
    }
}
