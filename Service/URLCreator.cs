﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace MetroChecker.Service
{
    public class URLCreator
    {
        static string key = "1d444025-5d0c-45ef-a65b-4d2223ba5e99";
        static string user_id = "3001928";
        static string metro_url = "https://timetableapi.ptv.vic.gov.au";

        public static string GetQualifiedUrl(string url)
        {
            url = string.Format("{0}{1}devid={2}", url, url.Contains("?") ? "&" : "?", user_id);
            ASCIIEncoding encoding = new();
            byte[] keyBytes = encoding.GetBytes(key);
            byte[] urlBytes = encoding.GetBytes(url);

            byte[] tokenBytes = new HMACSHA1(keyBytes).ComputeHash(urlBytes);

            StringBuilder stringBuilder = new();

            Array.ForEach<byte>(tokenBytes, x => stringBuilder.Append(x.ToString("X2")));

            string qualifiedURL = string.Format("{0}&signature={1}", url, stringBuilder.ToString());

            return metro_url + qualifiedURL;
        }
    }
}
