﻿using MetroChecker.Models.Routes;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace MetroChecker.Service
{
    public class MetroService : IMetroService
    {
        HttpClient client = new();

        private async Task<string> ProcessRequests(string url)
        {
            var finalUrl = URLCreator.GetQualifiedUrl(url);

            HttpResponseMessage response = await client.GetAsync(finalUrl);
            response.EnsureSuccessStatusCode();

            return await response.Content.ReadAsStringAsync();
        }

        public async Task<RouteTypes> GetRouteTypes(string url)
        {
            var result = await ProcessRequests(url);

            RouteTypes routeTypes = JsonConvert.DeserializeObject<RouteTypes>(result);

            return routeTypes;
        }

        public async Task<Routes> GetAllRoutes(string url)
        {
            var result = await ProcessRequests(url);

            Routes routes = JsonConvert.DeserializeObject<Routes>(result);
            
            return routes;
        }
    }
}
