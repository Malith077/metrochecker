﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MetroChecker.Models.Routes;

namespace MetroChecker.Service
{
    public interface IMetroService
    {
        Task<RouteTypes> GetRouteTypes(string url);
        Task<Routes> GetAllRoutes(string url);
    }
}
